#!/bin/sh

# background image
nitrogen --restore &
# picom
picom & 
# set mouse speed & keyboard setting
xinput --set-prop 8 153 0.5 0 0 0 0.5 0 0 0 1 &
xset mouse 0 0 &
xset r rate 200 25 &
xset r 66 &
# set change-language hot key
setxkbmap -layout "us,th" -variant "colemak," -option "grp:alt_shift_toggle" &
