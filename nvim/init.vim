source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/themes/airline.vim
source $HOME/.config/nvim/themes/onedark.vim
source $HOME/.config/nvim/vim-config/lsp-config.vim
luafile $HOME/.config/nvim/lua/plugins/compe-config.lua
luafile $HOME/.config/nvim/lua/lsp/python-ls.lua
luafile $HOME/.config/nvim/lua/plugins/lua-tree-config.lua
luafile $HOME/.config/nvim/lua/plugins/plugins.lua
source $HOME/.config/nvim/lua/plugins/lua-tree-config.vim
source $HOME/.config/nvim/vim-config/vim-config.vim
luafile $HOME/.config/nvim/lua/plugins/lualine-config.lua
luafile $HOME/.config/nvim/lua/plugins/buffer-line-config.lua


