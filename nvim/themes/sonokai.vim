if (has("autocmd") && !has("gui_running"))
  augroup colorset
    autocmd!
    let s:white = { "gui": "#ABB2BF", "cterm": "145", "cterm16" : "7" }
  augroup END
endif

if has('termguicolors')
	set termguicolors
endif

let g:sonokai_style = 'andromeda'
let g:sonokai_disable_italic_comment = 1

syntax on
colorscheme sonokai

