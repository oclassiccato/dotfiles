" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
	  \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	"autocmd VimEnter * PlugInstall
	"autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')
	" Better Syntax Support
	Plug 'sheerun/vim-polyglot'
  " File Explorer
	Plug 'kyazdani42/nvim-web-devicons' " for file icons
	Plug 'kyazdani42/nvim-tree.lua'
	" Auto pairs for '(' '[' '{'
	Plug 'jiangmiao/auto-pairs'
	" lualine
	Plug 'nvim-lualine/lualine.nvim'
	Plug 'kyazdani42/nvim-web-devicons'
	Plug 'akinsho/bufferline.nvim'
	" colorscheme
	Plug 'sainnhe/sonokai'
	Plug 'joshdick/onedark.vim'
	Plug 'shaunsingh/nord.nvim'
	" intelligent 
	Plug 'neovim/nvim-lspconfig'
	Plug 'hrsh7th/nvim-compe'
 	" vim snip	
	Plug 'hrsh7th/vim-vsnip'
	Plug 'hrsh7th/vim-vsnip-integ' 

call plug#end()
