" Use ctrl-[hjkl] to select the active split!
nnoremap <C-k> :wincmd k<CR>
nnoremap <C-h> :wincmd j<CR>
nnoremap <C-j> :wincmd h<CR>
nnoremap <C-l> :wincmd l<CR>

nnoremap <silent> <tab> :bnext<CR>
nnoremap <silent> <C-n> :NvimTreeToggle<CR>

set relativenumber 
set langmap=hk,jh,kj " colemak 
